using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Api.Controllers;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Xunit;

namespace Testing
{
  public class CollectionTesting
  {
    private Context Before { get; }
    private Context After { get; }

    public CollectionTesting()
    {
      string name = "test_db_" + new Random().Next();
      Before = GetContext(name);
      After = GetContext(name);
    }

    [Fact]
    public void CanStoreAndLocateCollections()
    {
      Before.Collections.AddRange(new Collection { Name = "uno" }, new Collection { Name = "duo" });
      Before.SaveChanges();
      Thread.Sleep(1000);

      Assert.Equal(2, After.Collections.Count());
      Assert.NotNull(After.Collections.Single(a => a.Name == "uno"));
      Assert.NotNull(After.Collections.Single(a => a.Name == "duo"));
    }

    [Fact]
    public void CanRetrieveAllCollections()
    {
      const int count = 5;
      const string name = "collection";
      List<Collection> collections = new List<Collection>();
      for (int i = 0; i < count; i++)
        collections.Add(new Collection { Name = name + i });

      Before.Collections.AddRange(collections);
      Before.SaveChanges();
      Thread.Sleep(1000);

      CollectionController controller = new CollectionController(After);
      OkObjectResult result = controller.Get().Result as OkObjectResult;
      IIncludableQueryable<Collection, Game> actuals
        = result.Value as IIncludableQueryable<Collection, Game>;

      Assert.Equal(count, actuals.Count());
      foreach (Guid id in collections.Select(a => a.Id))
        Assert.NotNull(actuals.Single(a => a.Id == id));
    }

    [Fact]
    public void CanRetrieveSpecificCollection()
    {
      const int count = 3;
      const string name = "collection";
      List<Collection> collections = new List<Collection>();
      for (int i = 0; i < count; i++)
        collections.Add(new Collection { Name = name + i });

      Before.Collections.AddRange(collections);
      Before.SaveChanges();
      Thread.Sleep(1000);

      CollectionController controller = new CollectionController(After);
      List<Collection> actuals = new List<Collection>();
      foreach (Guid id in collections.Select(a => a.Id))
      {
        OkObjectResult result = controller.Get(id).Result as OkObjectResult;
        actuals.Add(result.Value as Collection);
      }

      Assert.Equal(count, actuals.Count);
      foreach (Guid id in collections.Select(a => a.Id))
        Assert.NotNull(actuals.Single(a => a.Id == id));
    }

    private static Context GetContext(string name)
    {
      DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
        .UseInMemoryDatabase(name)
        .Options;

      return new Context(options);
    }
  }
}