using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Api.Controllers;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Testing
{
  public class GameTesting
  {
    private Context Before { get; }
    private Context After { get; }

    public GameTesting()
    {
      string name = "test_db_" + new Random().Next();
      Before = GetContext(name);
      After = GetContext(name);
    }

    [Fact]
    public void CanStoreAndLocateGames()
    {
      Before.Games.AddRange(new Game { Name = "alpha" }, new Game { Name = "beta" });
      Before.SaveChanges();
      Thread.Sleep(1000);

      Assert.Equal(2, After.Games.Count());
      Assert.NotNull(After.Games.Single(a => a.Name == "alpha"));
      Assert.NotNull(After.Games.Single(a => a.Name == "beta"));
    }

    [Fact]
    public void CanRetrieveAllGames()
    {
      const int count = 5;
      const string name = "game";
      List<Game> games = new List<Game>();
      for (int i = 0; i < count; i++)
        games.Add(new Game { Name = name + i });

      Before.Games.AddRange(games);
      Before.SaveChanges();
      Thread.Sleep(1000);

      GameController controller = new GameController(After);
      OkObjectResult result = controller.Get().Result as OkObjectResult;
      IIncludableQueryable<Game, IEnumerable<Device>> actuals
        = result.Value as IIncludableQueryable<Game, IEnumerable<Device>>;

      Assert.Equal(count, actuals.Count());
      foreach (Guid id in games.Select(a => a.Id))
        Assert.NotNull(actuals.Single(a => a.Id == id));
    }

    [Fact]
    public void CanRetrieveGamesByPage()
    {
      const int count = 6;
      const string name = "game";
      List<Game> games = new List<Game>();
      for (int i = 0; i < count; i++)
        games.Add(new Game { Name = name + i });

      Before.Games.AddRange(games);
      Before.SaveChanges();
      Thread.Sleep(1000);

      GameController controller = new GameController(After);
      OkObjectResult result1 = controller.GetByPage(0, 3).Result as OkObjectResult;
      IIncludableQueryable<Game, IEnumerable<Device>> actuals1
        = result1.Value as IIncludableQueryable<Game, IEnumerable<Device>>;
      OkObjectResult result2 = controller.GetByPage(1, 3).Result as OkObjectResult;
      IIncludableQueryable<Game, IEnumerable<Device>> actuals2
        = result2.Value as IIncludableQueryable<Game, IEnumerable<Device>>;

      Assert.Equal(actuals1.Count(), actuals2.Count());
      List<Game> actuals = actuals1.ToList().Concat(actuals2).ToList();
      foreach (Guid id in games.Select(a => a.Id))
        Assert.NotNull(actuals.Single(a => a.Id == id));
    }

    [Fact]
    public void CanRetrieveSpecificGame()
    {
      const int count = 3;
      const string name = "game";
      List<Game> games = new List<Game>();
      for (int i = 0; i < count; i++)
        games.Add(new Game { Name = name + i });

      Before.Games.AddRange(games);
      Before.SaveChanges();
      Thread.Sleep(1000);

      GameController controller = new GameController(After);
      List<Game> actuals = new List<Game>();
      foreach (Guid id in games.Select(a => a.Id))
      {
        OkObjectResult result = controller.Get(id).Result as OkObjectResult;
        actuals.Add(result.Value as Game);
      }

      Assert.Equal(count, actuals.Count);
      foreach (Guid id in games.Select(a => a.Id))
        Assert.NotNull(actuals.Single(a => a.Id == id));
    }

    [Fact]
    public void CanStartNewSession()
    {
      // todo Implement when authorization is ready.
      Assert.True(true);
    }

    [Fact]
    public void CanNotStartExistingSession()
    {
      // todo Implement when authorization is ready.
      Assert.True(true);
    }

    private static Context GetContext(string name)
    {
      DbContextOptions<Context> options = new DbContextOptionsBuilder<Context>()
        .UseInMemoryDatabase(name)
        .Options;

      return new Context(options);
    }
  }
}

