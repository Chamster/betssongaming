﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Api.Models;
using Microsoft.AspNetCore.Hosting;

namespace Api
{
  public static class Extensions
  {
    public static Guid Guidify(this string self)
    {
      // todo Manage invalid guids conversion attempts.
      return new Guid(self);
    }

    public static void Seed(this Context self, IHostingEnvironment environment)
    {
      Game game1 = new Game
      {
        Name = "Hakuna",
        Category = Category.BlackJack,
        Devices = new List<Device>
        {
          new Device { Name = "android" },
          new Device { Name = "laptop" }
        },
        ImageData = GetImageData(environment)
      };
      Game game2 = new Game
      {
        Name = "Matata",
        Category = Category.BlackJack,
        Devices = new List<Device>
        {
          new Device { Name = "android" },
          new Device { Name = "ios" },
          new Device { Name = "laptop" }
      },
        ImageData = GetImageData(environment)
      };
      Game game3 = new Game
      {
        Name = "Shazoo",
        Category = Category.ClassicSlots,
        Devices = new List<Device> { new Device { Name = "desktop" } },
        ImageData = GetImageData(environment)
      };

      self.Games.Add(game1);
      self.Games.Add(game2);
      self.Games.Add(game3);
      self.SaveChanges();

      Collection collection1 = new Collection
      {
        Name = "Black Jack Collection"
      };
      Collection collection2 = new Collection
      {
        Name = "Non Fruit Collection"
      };
      Collection collection3 = new Collection
      {
        Name = "Super Collection"
      };
      Collection collection4 = new Collection
      {
        Name = "Sub-uno Collection",
        Super = collection3
      };
      Collection collection5 = new Collection
      {
        Name = "Sub-duo Collection",
        Super = collection3
      };
      collection3.Subs = new[] { collection4, collection5 };

      self.Add(collection1);
      self.Add(collection2);
      self.Add(collection3);
      self.SaveChanges();

      Game_Mtm_Collection game_Mtm_Collection1 = new Game_Mtm_Collection
      {
        GameId = game1.Id,
        Game = game1,
        CollectionId = collection1.Id,
        Collection = collection1
      };
      Game_Mtm_Collection game_Mtm_Collection2 = new Game_Mtm_Collection
      {
        GameId = game2.Id,
        Game = game2,
        CollectionId = collection1.Id,
        Collection = collection1
      };
      Game_Mtm_Collection game_Mtm_Collection3 = new Game_Mtm_Collection
      {
        GameId = game1.Id,
        Game = game1,
        CollectionId = collection2.Id,
        Collection = collection2
      };
      Game_Mtm_Collection game_Mtm_Collection4 = new Game_Mtm_Collection
      {
        GameId = game3.Id,
        Game = game3,
        CollectionId = collection2.Id,
        Collection = collection2
      };

      self.Add(game_Mtm_Collection1);
      self.Add(game_Mtm_Collection2);
      self.Add(game_Mtm_Collection3);
      self.Add(game_Mtm_Collection4);
      self.SaveChanges();
    }

    private static byte[] GetImageData(IHostingEnvironment environment)
    {
      string contentPath = environment.ContentRootPath + "/face.png";
      Image image = Image.FromFile(contentPath);
      MemoryStream stream = new MemoryStream();
      image.Save(stream, ImageFormat.Png);
      // todo Remove the cut-off in the production version.
      return stream.ToArray().Take(50).ToArray();
    }
  }
}