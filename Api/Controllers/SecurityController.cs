﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Api.Controllers
{
  [Route("api/security")]
  [ApiController]
  public class SecurityController : ControllerBase
  {
    private Context Context { get; }

    public SecurityController(Context context)
    {
      Context = context;
    }

    [HttpGet("token"), AllowAnonymous]
    //public IActionResult RequestToken([FromBody] TokenRequest request)
    public ActionResult<string> RequestToken([FromQuery] string userName, [FromQuery] string password)
    {
      if (userName != "user1" || password != "pass1")
        return BadRequest($"Credentials USR: '{userName}', PWD: '{password}' are invalid.");

      // todo Use external config file for secrets.
      IEnumerable<Claim> claims = new[] { new Claim(ClaimTypes.Name, userName) };
      byte[] bytes = Encoding.UTF8.GetBytes("SecurityKeyOfProperLength");
      SymmetricSecurityKey key = new SymmetricSecurityKey(bytes);
      SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

      JwtSecurityToken token = new JwtSecurityToken(
        "localhost", "localhost", claims, null, DateTime.Now.AddMinutes(60), credentials);
      string output = new JwtSecurityTokenHandler().WriteToken(token);

      return Ok(output);
    }

    [HttpGet("test"), Authorize]
    public ActionResult<string> TestSecurity()
    {
      return "Token valid at " + DateTime.Now.ToString("HH:mm:ss");
    }
  }
}