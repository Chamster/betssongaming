﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
  [Route("api/games")]
  [ApiController]
  public class GameController : ControllerBase
  {
    private Context Context { get; }

    public GameController(Context context)
    {
      Context = context;
    }

    [HttpGet]
    public ActionResult<IEnumerable<Game>> Get()
    {
      return Ok(Context.Games
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Collection)
        .Include(_ => _.Devices));
    }

    [HttpGet("{index}/{size}")]
    public ActionResult<IEnumerable<Game>> GetByPage(int index, int size)
    {
      return Ok(Context.Games
        .Skip(index * size)
        .Take(size)
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Collection)
        .Include(_ => _.Devices));
    }

    [HttpGet("{id}")]
    public ActionResult<Game> Get(Guid id)
    {
      return Ok(Context.Games
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Collection)
        .Include(_ => _.Devices)
        .SingleOrDefault(a => a.Id == id));
    }

    [Authorize]
    [HttpPost]
    public ActionResult<Session> Post([FromBody] Guid gameId)
    {
      // todo Inject the manager as a service in the constructor.
      UserManager<object> manager = null;
      Guid userId = manager.GetUserId(User).Guidify();

      // todo Consider pulling out the logic for session start to an external method.
      Session session = Context.Sessions.SingleOrDefault(_
        => _.UserId == userId && _.GameId == gameId);

      // todo Inquire on the requirements for repeated session start request.
      if (session == null)
      {
        session = new Session { Id = Guid.NewGuid(), GameId = gameId, UserId = userId };
        Context.Sessions.Add(session);
        //Context.SaveChangesAsync();
        Context.SaveChanges();
      }
      return session;
    }
  }
}