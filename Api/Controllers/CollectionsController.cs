﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
  [Route("api/collections")]
  [ApiController]
  public class CollectionController : ControllerBase
  {
    private Context Context { get; }

    public CollectionController(Context context)
    {
      Context = context;
    }

    [HttpGet]
    public ActionResult<IEnumerable<Collection>> Get()
    {
      return Ok(Context.Collections
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Game));
    }

    [HttpGet("{index}/{size}")]
    public ActionResult<IEnumerable<Collection>> GetByPage(int index, int size)
    {
      return Ok(Context.Collections
        .Skip(index * size)
        .Take(size)
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Game));
    }

    [HttpGet("{id}")]
    public ActionResult<Collection> Get(Guid id)
    {
      return Ok(Context.Collections
        .Include(a => a.Game_Mtm_Collections)
        .ThenInclude(a => a.Game)
        .SingleOrDefault(a => a.Id == id));
    }
  }
}