﻿namespace Api.Models
{
  public enum Category
  {
    ClassicSlots,
    VideoSlots,
    Roulette,
    LiveRoulette,
    Poker,
    BlackJack
  }
}