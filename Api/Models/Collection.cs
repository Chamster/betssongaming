﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Models
{
  public class Collection
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<Game_Mtm_Collection> Game_Mtm_Collections { get; set; }
    public IEnumerable<Game> Games => Game_Mtm_Collections.Select(a => a.Game);
    public Collection Super { get; set; }
    public IEnumerable<Collection> Subs { get; set; }

    public Collection()
    {
      Game_Mtm_Collections = new List<Game_Mtm_Collection>();
      Subs= new List<Collection>();
    }
  }
}