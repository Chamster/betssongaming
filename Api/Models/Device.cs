﻿using System;

namespace Api.Models
{
  public class Device
  {
    public Guid Id { get; set; }
    public string  Name { get; set; }
    public byte[] Data { get; set; }
  }
}