﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Api.Models
{
    public class Game
    {
      public Guid Id { get; set; }
      public string Name { get; set; }
      public Category Category { get; set; }
      //public Image Image => new Bitmap(new MemoryStream(ImageData));
      public Image Image => null;
      public byte[] ImageData { get; set; }
      public IEnumerable<Device> Devices { get; set; }
      public IEnumerable<Game_Mtm_Collection> Game_Mtm_Collections { get; set; }
      public IEnumerable<Collection> Collections => Game_Mtm_Collections.Select(a => a.Collection);

      public Game()
      {
        Game_Mtm_Collections = new List<Game_Mtm_Collection>();
      }
    }
}
