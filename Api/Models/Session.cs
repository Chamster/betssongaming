﻿using System;

namespace Api.Models
{
  public class Session
  {
    public Guid Id { get; set; }
    public Guid GameId { get; set; }
    public Guid UserId { get; set; }
    // todo Implement a full URL including the game's ID as a parameter.
    public string Url => "/api/games/" + GameId;
  }
}