﻿using System;

namespace Api.Models
{
  public class Game_Mtm_Collection
  {
    public Guid GameId { get; set; }
    public Game Game { get; set; }
    public Guid CollectionId { get; set; }
    public Collection Collection { get; set; }
  }
}