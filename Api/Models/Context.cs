﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Models
{
  public sealed class Context : DbContext
  {
    public DbSet<Game> Games { get; set; }
    public DbSet<Collection> Collections { get; set; }
    public DbSet<Session> Sessions { get; set; }

    public Context() { }

    public Context(DbContextOptions<Context> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      EntityTypeBuilder<Game> game = builder.Entity<Game>();
      // todo Implement conversion between Image and byte[] for storage in a database: https://code.msdn.microsoft.com/How-to-save-Image-to-978a7b0b.
      game.Ignore(a => a.Image);
      game.Ignore(a => a.Collections);

      EntityTypeBuilder<Collection> collection = builder.Entity<Collection>();
      collection.HasMany(_ => _.Subs);
      collection.Ignore(_ => _.Games);

      EntityTypeBuilder<Game_Mtm_Collection> game_Mtm_Collection = builder.Entity<Game_Mtm_Collection>();
      game_Mtm_Collection.HasKey(a => new { a.GameId, a.CollectionId });
      game_Mtm_Collection.HasOne(a => a.Game)
        .WithMany(a => a.Game_Mtm_Collections)
        .HasForeignKey(a => a.GameId);
      game_Mtm_Collection.HasOne(_ => _.Collection)
        .WithMany(_ => _.Game_Mtm_Collections)
        .HasForeignKey(_ => _.CollectionId);
    }
  }
}
