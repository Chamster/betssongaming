﻿using System.Text;
using Api.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<Context>(a => a.UseInMemoryDatabase("donkey"));

      services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(a => a.TokenValidationParameters = GetParameters());

      services.AddMvc()
        .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)

        .AddJsonOptions(a =>
        {
          a.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
          a.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        });
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment environment)
    {
      if (environment.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        using (var serviceScope = app.ApplicationServices
          .GetRequiredService<IServiceScopeFactory>().CreateScope())
        {
          Context context = serviceScope.ServiceProvider.GetService<Context>();
          context.Seed(environment);
        }
      }
      else
        app.UseHsts();

      //app.UseFileServer(new FileServerOptions { EnableDirectoryBrowsing = true });
      app.UseAuthentication();
      app.UseHttpsRedirection();
      app.UseMvc();
    }

    private static TokenValidationParameters GetParameters()
    {
      return new TokenValidationParameters
      {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = "localhost",
        ValidAudience = "localhost",
        // todo Use external config file for secrets.
        IssuerSigningKey = new SymmetricSecurityKey(
          Encoding.UTF8.GetBytes("SecurityKeyOfProperLength"))
      };
    }
  }
}
